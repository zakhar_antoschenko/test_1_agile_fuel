class Feedback < ApplicationRecord
  belongs_to :experience
  has_many :responses

  after_create :update_experience_rating
  after_destroy :update_experience_rating

  def update_experience_rating
    calculated_rating =
<<-SQL
      (SELECT ROUND(AVG(CAST(substring(answer FROM '[0-9]+') AS INT)), #{Experience::RATING_PRECISION})
        FROM "responses"
       INNER JOIN "feedback" 
        ON "feedback"."id" = "responses"."feedback_id"
       WHERE "feedback"."experience_id" = #{experience_id})
SQL
    Experience.where(id: experience_id).update_all("rating = #{calculated_rating}")
  end

end