class Response < ApplicationRecord
  AVAILABLE_ANSWERS = ['1 awful',
                       '2 bad',
                       '3 not bad',
                       '4',
                       '4 OK',
                       '5 awesome',
                       '5 great'].freeze
  belongs_to :feedback

end