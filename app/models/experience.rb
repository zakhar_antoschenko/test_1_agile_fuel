class Experience < ApplicationRecord
  has_many :feedback
  RATING_PRECISION = 2.freeze

  def calculated_rating
    Response.joins(:feedback)
        .where(feedback: {experience_id: id})
        .average("CAST(substring(answer FROM '[0-9]+') AS INT)")
        .to_f
        .round(RATING_PRECISION)
  end
end