FactoryBot.define do
  factory :experience do
    name {Faker::Lorem.word}
    factory :experience_with_feedback_and_responses do
      transient do
        feedbacks_count {5}
      end

      after(:create) do |experience, evaluator|
        create_list(:feedback_with_responses, evaluator.feedbacks_count, experience: experience)
      end
    end
  end
end
