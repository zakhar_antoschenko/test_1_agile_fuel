FactoryBot.define do
  factory :response do
    association :feedback
    question { 'Rate the experience'}
    answer   { Response::AVAILABLE_ANSWERS.sample }

  end
end