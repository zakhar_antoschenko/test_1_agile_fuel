FactoryBot.define do
  factory :feedback do
    association :experience

    factory :feedback_with_responses do
      transient do
        responses_count {5}
      end

      after(:create) do |feedback, evaluator|
        create_list(:response, evaluator.responses_count, feedback: feedback)
      end
    end
  end
end