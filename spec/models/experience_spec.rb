describe Experience, type: :model do
  context '#calculated_rating' do
    context 'valid responses' do

      let(:experiences) {create_list(:experience_with_feedback_and_responses, 6)}

      it 'returns correct calculated_rating when all marks are the same(==5)' do
        experience = experiences.last
        Response.joins(:feedback)
            .where("feedback.experience_id = :exp_id", exp_id: experience.id)
            .update_all(answer: Response::AVAILABLE_ANSWERS.last)
        expect(experience.calculated_rating).to eq 5
      end

      it 'returns correct calculated_rating when all marks are the same(==3)' do
        experience = experiences.last
        Response.joins(:feedback)
            .where("feedback.experience_id = :exp_id", exp_id: experience.id)
            .update_all(answer: Response::AVAILABLE_ANSWERS[2])
        expect(experience.calculated_rating).to eq 3
      end

      context 'experience with the different marks' do
        let!(:exp_1) {create(:experience_with_feedback_and_responses)}
        let!(:exp_2) {create(:experience)}
        let!(:feedback_21) {create(:feedback, experience: exp_2)}
        let!(:feedback_22) {create(:feedback, experience: exp_2)}

        # first feedback
        # 5 + 2 + 4 + 1 + 5 + 3
        let!(:response_11) do
          create(:response,
                 feedback: feedback_21,
                 answer: Response::AVAILABLE_ANSWERS[6])
        end
        let!(:response_12) do
          create(:response,
                 feedback: feedback_21,
                 answer: Response::AVAILABLE_ANSWERS[1])
        end
        let!(:response_13) do
          create(:response,
                 feedback: feedback_21,
                 answer: Response::AVAILABLE_ANSWERS[3])
        end
        let!(:response_14) do
          create(:response,
                 feedback: feedback_21,
                 answer: Response::AVAILABLE_ANSWERS[0])
        end
        let!(:response_15) do
          create(:response,
                 feedback: feedback_21,
                 answer: Response::AVAILABLE_ANSWERS[5])
        end
        let!(:response_16) do
          create(:response,
                 feedback: feedback_21,
                 answer: Response::AVAILABLE_ANSWERS[2])
        end


        # second feedback
        # 5 + 4
        let!(:response_21) do
          create(:response,
                 feedback: feedback_22,
                 answer: Response::AVAILABLE_ANSWERS[6])
        end
        let!(:response_22) do
          create(:response,
                 feedback: feedback_22,
                 answer: Response::AVAILABLE_ANSWERS[4])
        end

        it 'returns correct calculated_rating when all marks are different' do
          expect(exp_2.calculated_rating).to eq 3.63
        end
      end
    end
  end

  context '#rating from database' do
    context 'triggering on records destroy and create' do

      context 'with the different marks' do
        let!(:exp_2)      {create(:experience)}

        # responses for the first feedback
        # 5 + 2 + 4 + 1 + 5 + 3
        let!(:response_11) { create(:response, answer: Response::AVAILABLE_ANSWERS[6])}
        let!(:response_12) { create(:response, answer: Response::AVAILABLE_ANSWERS[1])}
        let!(:response_13) { create(:response, answer: Response::AVAILABLE_ANSWERS[3])}
        let!(:response_14) { create(:response, answer: Response::AVAILABLE_ANSWERS[0])}
        let!(:response_15) { create(:response, answer: Response::AVAILABLE_ANSWERS[5])}
        let!(:response_16) { create(:response, answer: Response::AVAILABLE_ANSWERS[2])}

        # responses for second feedback
        # 5 + 4
        let(:response_21) { build(:response, answer: Response::AVAILABLE_ANSWERS[6])}
        let(:response_22) { build(:response, answer: Response::AVAILABLE_ANSWERS[4])}

        let(:feedback_21) { create(:feedback, experience: exp_2,
                                  responses: [response_11, response_12,
                                              response_13, response_14,
                                              response_15, response_16])
        }
        let(:feedback_22) { create(:feedback,experience: exp_2,
                                   responses: [response_22, response_21])
        }



        context '#create' do
          it 'recalculates rating when feedback created' do
            #create first feedback
            feedback_21
            expect do
              feedback_22
            end.to change {exp_2.reload.rating}.from(3.33).to(3.63)
          end
        end

        context '#destroy' do
          it 'recalculates rating when feedback created' do
            feedback_21
            feedback_22
            expect do
              feedback_22.destroy
            end.to change {exp_2.reload.rating}.from(3.63).to(3.33)
          end
        end
      end
    end
  end

end