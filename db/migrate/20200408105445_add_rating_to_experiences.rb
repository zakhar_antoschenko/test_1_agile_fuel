class AddRatingToExperiences < ActiveRecord::Migration[5.1]
  def change
    add_column :experiences, :rating, :float
  end
end
