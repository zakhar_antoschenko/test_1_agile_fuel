class CreateResponses < ActiveRecord::Migration[5.1]
  def change
    create_table :responses do |t|
      t.integer :feedback_id
      t.string :question
      t.string :answer
    end
  end
end