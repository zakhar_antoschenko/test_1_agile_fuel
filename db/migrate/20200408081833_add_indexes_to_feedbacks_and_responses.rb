class AddIndexesToFeedbacksAndResponses < ActiveRecord::Migration[5.1]
  def change
    add_index :feedback, :experience_id
    add_index :responses, :feedback_id
  end
end