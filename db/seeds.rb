# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
10.times do |i|
  experience = Experience.create
  puts("Created exp #{i}")
  20.times do |j|
    feedback = Feedback.create(experience: experience)
    50.times do |k|
      Response.create(feedback: feedback,
                      question: 'Just a question',
                      answer: Response::AVAILABLE_ANSWERS.sample)

    end
  end
end